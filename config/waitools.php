<?php

return [
    'app' => [
        'email' => env('APP_EMAIL', 'example@domain.tld'),
        'name' => env('APP_NAME', 'Default application name'),
    ],

    'mail' => [
        'logo' => env('MAIL_LOGO', 'images/mail_logo.jpg'),
        'cover' => env('MAIL_COVER', 'images/mail_cover.jpg'),
        'color' => env('MAIL_COLOR', '#DA2128'),
    ],

    'cms' => [
        'url' => env('CMS_URL', 'http://cmstest.contactmedia.co.za/api'),
        'host' => env('CMS_HOST'),
        'username' => env('CMS_USERNAME'),
        'password' => env('CMS_PASSWORD'),

        'defaultPagination' => env('CMS_PAGINATION', 5),
    ],

    'issues' => [
        'url' => env('ISSUES_URL', 'http://mags.contactmedia.co.za/api'),
        'username' => env('ISSUES_USERNAME', 'username'),
        'password' => env('ISSUES_PASSWORD', 'password'),

        'identifier' => env('ISSUES_IDENTIFIER', 'afropolitan'),
        'cache' => env('ISSUES_CACHE', 24),
    ],

    'mailchimp' => [
        'key' => env('MC_KEY', ''),
        'list' => env('MC_LIST', ''),
    ],

    'nuz' => [
        'url' => env('NUZ_URL', ''),
        'key' => env('NUZ_KEY', ''),
        'list' => env('NUZ_LIST', ''),
    ],

    'emailer' => [
        'key' => env('EMAILER_KEY', ''),
        'secret' => env('EMAILER_SECRET', ''),
        'url' => env('EMAILER_URL', ''),
    ],

    'ads' => [
        'google' => [
            'client_id' => env('GOOGLE_ADDS_CLIENT_ID', ''),
        ],
    ],

    'recaptcha' => [
        'key' => env('RECAPTCHA_KEY', ''),
        'secret' => env('RECAPTCHA_SECRET', ''),
    ],
];

