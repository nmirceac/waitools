<?php namespace WAI;

use WAI\Api;
use WAI\Exception;

/**
 * Class Cms
 * @package App
 */
class Cms
{
    public $instance = null;
    public $application = null;
    public static $applicationObject = null;

    public static $staticInstance = null;

    const TYPE_ITEM = 0;
    const TYPE_APPLICATION = 1;
    const TYPE_CATEGORY = 2;
    const TYPE_ARTICLE = 3;
    const TYPE_COMMENT = 4;
    const TYPE_AUTHOR = 5;
    const TYPE_SPEAKER = 6;
    const TYPE_SPONSOR = 7;
    const TYPE_PAGE = 8;
    const TYPE_PRINT = 9;
    const CATEGORY_MENU_ITEM = 1;
    const CATEGORY_MENU_EXPAND_CATEGORIES = 2;
    const CATEGORY_MENU_EXPAND_ARTICLES = 3;
    const CATEGORY_TYPE_ARTICLES = 3;
    const CATEGORY_TYPE_PAGES = 8;
    const CATEGORY_TYPE_PRINT = 9;

    function __construct($id = null)
    {
        $this->url = config('waitools.cms.url');
        $this->host = config('waitools.cms.host');
        $this->username = config('waitools.cms.username');
        $this->password = config('waitools.cms.password');
        $this->application_id = $id;

        if (empty($this->url)) {
            throw new Exception('No API url configured');
        }

        if (empty($this->host)) {
            throw new Exception('No API host configured');
        }

        if (empty($this->username)) {
            throw new Exception('No API username configured');
        }

        if (empty($this->password)) {
            throw new Exception('No API password configured');
        }

        $this->instance = new Api($this->url, $this->username, $this->password, [
            'debug' => false,
            'load_constants' => true
        ]);

        $this->getApplication();
    }

    function getApplication()
    {
        $fromCache = \Cache::get('getApplication-' . $this->host . '-' . $this->application_id);
        if (is_null($fromCache)) {
            if (!is_null($this->application_id)) {
                try {
                    if (is_null($this->application)) {
                        $this->application = $this->instance->get_application($this->application_id);
                    }
                } catch (\Exception $e) {
                    throw new Exception('The application with ID ' . $this->application_id . ' was not found on the CMS');
                }
            } else {
                try {
                    if (is_null($this->application)) {
                        $this->application = $this->instance->get_application_by_host($this->host);
                    }
                } catch (\Exception $e) {
                    throw new Exception('The application for the host ' . $this->host . ' was not found on the CMS');
                }
            }
            \Cache::put('getApplication-' . $this->host . '-' . $this->application_id, $this->application, \Carbon\Carbon::now()->addMinutes(120));
        } else {
            $this->application = $fromCache;
        }

        return $this->application;
    }

    public function getAdverts()
    {
        $adverts = [];
        $cmsAdverts = $this->application['services']['openx'];
        foreach ($cmsAdverts as $advert) {
            $advert = explode('=', $advert);
            $adverts[$advert[0]] = $advert[1];
        }

        return $adverts;
    }

    function getApplications()
    {
        return $this->instance->get_applications();
    }

    /**
     * @param int $parentId
     * @param array $options
     * @return mixed
     */
    function getCategories($parentId = 0, $options = [])
    {
        if ($parentId === 0) {
            $parentId = $this->application['id'];
        }

        $showInMenu = null;
        if (isset($options['show_in_menu'])) {
            if ($options['show_in_menu']) {
                $showInMenu = true;
            } else {
                $showInMenu = false;
            }
            unset($options['show_in_menu']);
        }

        $categories = $this->instance->get_categories($parentId, $options);
        if (!is_null($showInMenu)) {
            foreach ($categories as $categoryId => $category) {
                if ($showInMenu and !(isset($category['show_in_menu']) and $category['show_in_menu'])) {
                    unset($categories[$categoryId]);
                } elseif (!$showInMenu and isset($category['show_in_menu']) and $category['show_in_menu']) {
                    unset($categories[$categoryId]);
                }
            }
        }

        return array_values($categories);
    }

    function getCategoriesAndSubcategories($parentId = 0, $options = [])
    {

        if (!isset($options['type'])) {
            $options['type'] = self::CATEGORY_TYPE_ARTICLES;
        }

        $data = $this->getCategories($parentId, ['depth' => 2, 'load_paths' => true, 'type' => $options['type']]);

        $categories = [];

        foreach ($data as $category) {
            if (is_null($category['primary_path'])) {
                $category['subcategories'] = [];
                $categories[$category['id']] = $category;
            }
        }

        foreach ($data as $category) {
            if (!is_null($category['primary_path'])) {
                if (isset($category['primary_path'][0]['id'])) {
                    $categories[$category['primary_path'][0]['id']]['subcategories'][$category['id']] = $category;
                }
            }
        }

        $showInMenu = null;
        if (isset($options['show_in_menu'])) {
            if ($options['show_in_menu']) {
                $showInMenu = true;
            } else {
                $showInMenu = false;
            }
            unset($options['show_in_menu']);
        }

        if (!is_null($showInMenu)) {
            foreach ($categories as $categoryId => $category) {
                if ($showInMenu and !(isset($category['show_in_menu']) and $category['show_in_menu'])) {
                    unset($categories[$categoryId]);
                } elseif (!$showInMenu and isset($category['show_in_menu']) and $category['show_in_menu']) {
                    unset($categories[$categoryId]);
                }
            }
        }

        return array_values($categories);
    }

    /**
     * @param $query
     * @param int $articlesPerPage
     * @param null $page
     * @return mixed
     */
    function searchArticles($query, $articlesPerPage = null, $page = null, $categoryId = null)
    {
        if (is_null($page)) {
            $page = request()->get('page', 1);
        }

        if (is_null($categoryId)) {
            $categoryId = $this->getApplication()['id'];
        }

        if ($articlesPerPage == null) {
            $articlesPerPage = config('waitools.cms.defaultPagination');
        }

        return $this->instance->get_articles(['parents' => $categoryId, 'query' => $query, 'limit' => $articlesPerPage, 'page' => $page]);
    }

    /**
     * @param int $path
     * @param bool $getSchema
     * @return array
     */
    function getArticlesByCategoryPath($path = 0, $options = [])
    {
        if ($path === 0) {
            return [];
        }

        $resolvePath = $this->instance->resolve_path($path, ['application' => $this->getApplication()['id']]);

        if (isset($resolvePath['id']) and isset($resolvePath['type']) and $resolvePath['type'] == self::TYPE_CATEGORY) {
            return $this->getArticlesByParentId($resolvePath['id'], $options);
        } else {
            return [];
        }
    }

    function resolvePath($path = 0)
    {
        try {
            $resolvePath = $this->instance->resolve_path($path, ['application' => $this->getApplication()['id']]);
        } catch (\Exception $e) {
            throw new Exception('The path ' . $path . ' was not found  in the application ' . $this->host);
        }

        return $resolvePath;
    }

    /**
     * @param int $path
     * @param bool $getSchema
     * @return array
     */
    function getPagesByCategoryPath($path = 0, $getSchema = false)
    {
        if ($path === 0) {
            return [];
        }

        $resolvePath = $this->resolvePath($path);

        if (isset($resolvePath['id']) and isset($resolvePath['type']) and $resolvePath['type'] == self::TYPE_CATEGORY) {
            $pages = [];
            foreach ($this->instance->get_pages(['parents' => $resolvePath['id'], 'load_schema' => $getSchema])['pages'] as $page) {
                $page['contentArray'] = self::getContentArray($page);
                $pages[$page['short']] = $page;
            }
            return $pages;
        } else {
            return [];
        }
    }

    /**
     * @param int $path
     * @param bool $getSchema
     * @return array
     */
    function getCategoryByCatergoryPath($path = 0, $getSchema = false)
    {
        if ($path === 0) {
            return [];
        }

        $resolvePath = $this->resolvePath($path);

        if (isset($resolvePath['id']) and isset($resolvePath['type']) and $resolvePath['type'] == self::TYPE_CATEGORY) {
            return $resolvePath;
        } else {
            return [];
        }
    }


    /**
     * @param int $parentId
     * @param int $articlesPerPage
     * @param null $page
     * @return mixed
     */
    function getPaginatedArticles($parentId = 0, $articlesPerPage = null, $page = null)
    {
        if (is_null($page)) {
            $page = request()->get('page', 1);
        }

        if ($articlesPerPage == null) {
            $articlesPerPage = config('waitools.cms.defaultPagination');
        }

        $options = ['limit' => $articlesPerPage, 'page' => $page];

        return $this->getArticlesByParentId($parentId, $options);
    }

    /**
     * @param int $parentId
     * @param bool $getSchema
     * @return mixed
     */
    function getArticlesByParentId($parentId = 0, $options = [])
    {
        if ($parentId === 0) {
            $parentId = $this->application['id'];
        }

        $options['parents'] = $parentId;
        $options['load_paths'] = true;
        $options['load_schema'] = true;

        if (!isset($options['limit'])) {
            $options['limit'] = config('waitools.cms.defaultPagination');
        }

        $data = $this->instance->get_articles($options);
        foreach ($data['articles'] as $a => $article) {
            if (!is_array($article)) {
                dd('oh shit');
            }
            $data['articles'][$a]['contentArray'] = self::getContentArray($article);

            $currentApplicationId = $this->application['id'];
            if (isset($data['articles'][$a]['primary_path'][0]['application']) and $data['articles'][$a]['primary_path'][0]['application'] != $currentApplicationId) {
                foreach ($data['articles'][$a]['secondary_paths'] as $secondaryPathId => $secondaryPath) {
                    if ($secondaryPath[0]['application'] == $currentApplicationId) {
                        $data['articles'][$a]['secondary_paths'][] = $data['articles'][$a]['primary_path'];
                        unset($data['articles'][$a]['secondary_paths'][$secondaryPathId]);
                        $data['articles'][$a]['primary_path'] = $secondaryPath;
                        break;
                    }
                }
                $data['articles'][$a]['secondary_paths'] = array_values($data['articles'][$a]['secondary_paths']);
            }
        }

        return $data;
    }

    /**
     * @param int $parentId
     * @param bool $getSchema
     * @return mixed
     */
    function getFeaturedArticlesByParentId($parentId = 0)
    {
        $options = ['featured' => true];
        return $this->getArticlesByParentId($parentId, $options);
    }

    /**
     * @param int $parentId
     * @param bool $getSchema
     * @return mixed
     */
    function getPagesByParentId($parentId = 0, $getSchema = false)
    {
        return $this->instance->get_pages(['parents' => $parentId, 'load_paths' => true, 'load_schema' => $getSchema])['pages'];
    }

    /**
     * @param int $parentId
     * @param bool $getSchema
     * @return mixed
     */
    function getPageById($id = 0, $getSchema = false)
    {
        return $this->instance->get_page($id, ['load_paths' => true, 'load_schema' => $getSchema]);
    }

    /**
     * @param int $path
     * @param bool $getSchema
     * @return array
     */
    function getCategoryByCategoryPath($path = 0, $includeSubcategories = false)
    {
        if ($path === 0) {
            return [];
        }

        $resolvePath = $resolvePath = $this->resolvePath($path);

        if (!(isset($resolvePath['id']) and isset($resolvePath['type']) and $resolvePath['type'] == self::TYPE_CATEGORY)) {
            return [];
        }

        if ($includeSubcategories) {
            return $this->getCategoriesAndSubcategories($resolvePath['id'], ['type' => self::CATEGORY_TYPE_PAGES]);
        } else {
            return $this->instance->get_category($resolvePath['id']);
        }
    }

    /**
     * @param int $articleId
     */
    function hitArticle($articleId = 0)
    {
        $this->instance->hit_article($articleId, true);
    }

    /**
     * @param int $path
     * @return array
     */
    function getItemByPath($path = 0, $getSchema = false)
    {
        if ($path === 0) {
            return [];
        }

        $resolvePath = $this->resolvePath($path);
        if (isset($resolvePath['id'])) {
            switch ($resolvePath['type']) {
                case $this->instance->TYPE_PAGE:
                    $item = $this->instance->get_page($resolvePath['id'], ['load_image_meta' => false, 'load_schema' => $getSchema]);
                    break;

                case $this->instance->TYPE_ARTICLE:
                    $item = $this->instance->get_article($resolvePath['id'], ['load_schema' => $getSchema]);
                    break;


                default:
                    break;
            }
            $item['contentArray'] = self::getContentArray($item);

            return $item;
        } else {
            return [];
        }

        throw new Exception('The unknown item type for item with id ' . $item['id'] . ' - ' . json_encode($item));
    }

    public function getTrendingArticles($options = [])
    {
        if (!isset($options['limit'])) {
            $options['limit'] = 4;
        }

        if (!isset($options['offset'])) {
            $options['offset'] = 0;
        }

        if (!isset($options['days'])) {
            $options['days'] = 5;
        }

        $trendingArticles = ['articles' => []];
        try {
            $trendingArticles = $this->instance->get_articles(['parents' => $this->getApplication()['id'], 'hits_days' => $options['days'], 'load_paths' => true, 'limit' => $options['limit']]);
        } catch (\Exception $e) {
        }

        return $trendingArticles;
    }

    /**
     * @param int $applicationId
     * @return mixed
     */
    public function getSponsors($applicationId = 0)
    {
        if ($applicationId === 0) {
            $applicationId = $this->application['id'];
        }

        $data = $this->instance->get_sponsors($applicationId);

        $activeSponsors = [];
        $order = [];
        foreach ($data as $sponsor) {
            if ($sponsor['status'] == 1) {
                $activeSponsors[] = $sponsor;
                $order[] = $sponsor['order'];
            }
        }
        array_multisort($order, SORT_ASC, SORT_NUMERIC, $activeSponsors);

        return $activeSponsors;
    }

    /**
     * @param int $applicationId
     * @return mixed
     */
    public function getSpeakers($applicationId = 0)
    {
        if ($applicationId === 0) {
            $applicationId = $this->application['id'];
        }

        $data = $this->instance->get_speakers($applicationId);
        $order = [];
        foreach ($data as $sponsor) {
            $order[] = $sponsor['order'];
        }
        array_multisort($order, SORT_ASC, SORT_NUMERIC, $data);

        return $data;
    }

    /**
     * @param $imgObject
     * @param array $options
     * @return string
     */
    public static function imgSrc($imgObject, $options = [])
    {
        if (!is_array($imgObject)) {
            return '';
            throw new \Exception('Invalid image object - the image object is not an array - ' . json_encode($imgObject));
        }

        if (isset($imgObject['layout'])) {
            $layout = substr($imgObject['layout'], 0, 1);
        } else {
            $layout = 'l';
        }

        if (isset($options[$layout])) {
            $options = $options[$layout];
        }


        $size = '';
        if (isset($options['w'])) {
            $size = $options['w'] . 'x';
        }

        if (isset($options['h'])) {
            if (empty($size)) {
                $size = 'x' . $options['h'];
            } else {
                $size .= $options['h'];
            }
        }

        if (!empty($size)) {
            $anchor = $imgObject['anchor'];
            if (isset($options['a'])) {
                $anchor = $options['a'];
            }

            if ($anchor != 'nocrop') {
                $size = '.' . $size . '|' . $anchor;
            } else {
                $size = '.' . $size;
            }
        }

        $type = $imgObject['type'];
        if (isset($options['f'])) {
            $options['f'] = strtolower($options['f']);

            if ($options['f'] == 'jpg' or $options['f'] == 'jpeg') {
                $type = 'jpeg';
            }

            if ($options['f'] == 'png') {
                $type = 'png';
            }
        }


        $imgSrc = $imgObject['src'] . $size . '.' . $type;


        return $imgSrc;
    }

    /**
     * @param $content
     * @return array
     */
    public static function getContentArray($article)
    {
        $content = $article['content'];
        $content = str_replace(
            ['<blockquote>' . PHP_EOL, PHP_EOL . '</blockquote>'],
            ['<blockquote>', '</blockquote>'],
            $content);
        preg_match_all("/^<\w+>(.*)<\/\w+>|<hr\s?\/?>|<\/?[o,u]l>$/m", $content, $parts);

        $output = [];
        $hasBlockQuote = false;

        $totalCharacters = 0;
        $totalParagraphs = 0;
        $inlineImages = [];

        $parts = $parts[0];
        foreach ($parts as $p => $part) {
            $part = trim($part);
            if (empty($part)) {
                unset($parts[$p]);
                continue;
            }

            $closingTag = substr($part, -3, -1);

            if (!in_array($closingTag, ['hr', 'ol', 'ul']) and empty(trim(html_entity_decode(strip_tags($part)), "\xC2\xA0\n\r\t "))) {
                unset($parts[$p]);
                continue;
            }

            $part = trim($part);
            preg_match("/^<(\w+)>(.*)<\/\w+>|<(hr)\s?\/>|<\/?([o,u]l)>$/m", $part, $part);

            if (count($part) > 3) {
                $part[1] = array_pop($part);
                $part = array_slice($part, 0, 3);
            }


            if ($part[1] == 'blockquote') {
                $hasBlockQuote = true;
            }

            $tag = $part[1];
            $content = $part[2];
            $full = $part[0];
            $stripped = strip_tags($content);

            if (substr($content, 0, 8) == '|inline|') {
                //inline photo
                $imageInfo = array_slice(explode('|', $content), 2, -1);
                $requiredImageInfo = ['src', 'layout', 'anchor', 'type', 'name', 'caption', 'align'];

                $result = [];
                foreach ($requiredImageInfo as $index => $value) {
                    if(isset($imageInfo[$index])) {
                        $result[$value] = $imageInfo[$index];
                    }
                }

                $imageInfo = $result;

                $output[] = [
                    'tag' => 'img',
                    'image' => $imageInfo,
                    'content' => null,
                    'full' => null
                ];

                $inlineImages[] = $imageInfo['src'];
            } else if (substr($stripped, 0, 32) == 'https://www.youtube.com/watch?v=') {
                //inline youtube
                $videoId = substr($stripped, 32);
                $videoId = explode('?', $videoId)[0];

                $output[] = [
                    'tag' => 'youtube',
                    'id' => $videoId,
                    'content' => null,
                    'full' => null
                ];
            } else if (substr($stripped, 0, 18) == 'https://vimeo.com/') {
                //inline vimeo
                $videoId = substr($stripped, 18);
                $videoId = explode('?', $videoId)[0];

                $output[] = [
                    'tag' => 'vimeo',
                    'id' => $videoId,
                    'content' => null,
                    'full' => null
                ];
            } else if (substr($stripped, 0, 34) == 'https://www.facebook.com/watch/?v=') {
                //inline facebook
                $videoId = substr($stripped, 34);
                $videoId = explode('?', $videoId)[0];

                $output[] = [
                    'tag' => 'facebookVideo',
                    'id' => $videoId,
                    'content' => null,
                    'full' => null
                ];
            } else if (substr($stripped, 0, 25) == 'https://www.facebook.com/' and strpos($stripped, '/videos/')) {
                //inline facebook
                $videoId = trim($stripped, '/');
                $videoId = substr($videoId, 1 + strrpos($videoId, '/'));
                $videoId = explode('?', $videoId)[0];

                $output[] = [
                    'tag' => 'facebookVideo',
                    'id' => $videoId,
                    'content' => null,
                    'full' => null
                ];
            } else {
                $output[] = [
                    'tag' => $tag,
                    'content' => $content,
                    'full' => $full
                ];

                if ($tag == 'p') {
                    $totalCharacters += strlen($content);
                    $totalParagraphs++;
                }
            }
        }

        $galleryImages = [];
        foreach ($article['images'] as $image) {
            if ($image['main']) {
                continue;
            }
            if (in_array($image['src'], $inlineImages)) {
                continue;
            }

            $galleryImages[] = $image;
        }

        $articleParts = [
            'meta' => [
                'hasBlockQuote' => $hasBlockQuote,
                'totalCharacters' => $totalCharacters,
                'totalParagraphs' => $totalParagraphs,
                'inlineImages' => count($inlineImages),
                'galleryImages' => $galleryImages
            ],
            'parts' => $output
        ];

        return $articleParts;
    }

    public static function getPaginationArray($meta)
    {
        $baseUrl = explode('?', request()->getRequestUri())[0];
        if ($meta['last_page'] == 1) {
            return null;
        }

        $appendSuffix = '';
        $searchQuery = request()->get('q');
        if ($searchQuery) {
            $appendSuffix = '&q=' . $searchQuery;
        }

        $pages = [];

        $page = ['label' => 'Previous', 'p' => '-', 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($meta['current_page'] - 1) . $appendSuffix];
        if ($meta['current_page'] == 1) {
            $page['disabled'] = true;
        }
        $pages[] = $page;


        if ($meta['last_page'] <= 5) {
            for ($p = 1; $p <= $meta['last_page']; $p++) {
                $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                if ($meta['current_page'] == $p) {
                    $page['active'] = true;
                }
                $pages[] = $page;
            }
        } else {
            if ($meta['current_page'] < 4) {
                for ($p = 1; $p <= $meta['current_page'] + 1; $p++) {
                    $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                    if ($meta['current_page'] == $p) {
                        $page['active'] = true;
                    }
                    $pages[] = $page;
                }

                $page = ['label' => 'spacer', 'p' => '...', 'disabled' => false, 'active' => false, 'url' => null];
                $pages[] = $page;

                for ($p = $meta['last_page'] - 1; $p <= $meta['last_page']; $p++) {
                    $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                    if ($meta['current_page'] == $p) {
                        $page['active'] = true;
                    }
                    $pages[] = $page;
                }
            } else if ($meta['current_page'] > $meta['last_page'] - 3) {
                for ($p = 1; $p <= 3; $p++) {
                    $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                    if ($meta['current_page'] == $p) {
                        $page['active'] = true;
                    }
                    $pages[] = $page;
                }

                $page = ['label' => 'spacer', 'p' => '...', 'disabled' => false, 'active' => false, 'url' => null];
                $pages[] = $page;

                for ($p = $meta['current_page'] - 1; $p <= $meta['last_page']; $p++) {
                    $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                    if ($meta['current_page'] == $p) {
                        $page['active'] = true;
                    }
                    $pages[] = $page;
                }
            } else {
                $page = ['label' => 1, 'p' => 1, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . (1) . $appendSuffix];
                $pages[] = $page;

                $page = ['label' => 'spacer', 'p' => '...', 'disabled' => false, 'active' => false, 'url' => null];
                $pages[] = $page;

                for ($p = $meta['current_page'] - 2; $p <= min($meta['current_page'] + 2, $meta['last_page']); $p++) {
                    $page = ['label' => $p, 'p' => $p, 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($p) . $appendSuffix];
                    if ($meta['current_page'] == $p) {
                        $page['active'] = true;
                    }
                    $pages[] = $page;
                }

                $page = ['label' => 'spacer', 'p' => '...', 'disabled' => false, 'active' => false, 'url' => null];
                $pages[] = $page;

                $page = ['label' => $meta['last_page'], 'p' => $meta['last_page'], 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($meta['last_page']) . $appendSuffix];
                $pages[] = $page;
            }
        }

        $page = ['label' => 'Next', 'p' => '+', 'disabled' => false, 'active' => false, 'url' => $baseUrl . '?page=' . ($meta['current_page'] + 1) . $appendSuffix];
        if ($meta['current_page'] == $meta['last_page']) {
            $page['disabled'] = true;
        }
        $pages[] = $page;

        return $pages;
    }
}
