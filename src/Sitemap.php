<?php namespace WAI;

/**
 * Class Sitemap
 * @package WAI
 */
class Sitemap
{
    public function getSitemap()
    {
        $menu = \App\Menu::getMenu();
        $sitemap = new \Roumen\Sitemap\Sitemap(['use_cache'=>true, 'cache_key'=>'sitemap', 'cacheDuration'=>3600]);

        $sitemap->add(\URL::to('/'), date('r', time()), '1.0', 'daily');
        foreach($menu as $item) {
            if($item['url']=='/') {
                continue;
            }

            $priority = 0.8;
            $frequency = 'weekly';

            if(in_array($item['url'], ['/contact', '/issues'])) {
                $priority=0.5;
                $frequency='monthly';
            }

            $sitemap->add(\URL::to($item['url']), date('r', time()), $priority, $frequency);

            if(isset($item['children']) and !empty($item['children'])) {
                foreach($item['children'] as $child) {
                    $sitemap->add(\URL::to($child['url']), date('r', time()), $priority, $frequency);
                }
            }
        }

        $articles = \Cms::articlesByParentId(0, ['limit'=>1500])['articles'];
        foreach($articles as $article) {
            $images = [];
            foreach($article['images'] as $image) {
                $images[] = [
                    'url'=>imgSrc($image, ['w'=>250, 'h'=>250]),
                    'title'=>$image['name'],
                    'caption'=>$image['caption']
                ];
            }

            $videos = [];
//            shit support
//            if(isset($article['videos'])) {
//                foreach($article['videos'] as $video) {
//                    $videos[] = [
//                        'url'=>$video['url'],
//                        'title'=>$video['title'],
//                    ];
//                }
//            }

            $sitemap->add(\URL::to(route('article', $article['path'], false)), date('r', $article['updated_at']), '0.5', 'monthly', $images, [], [], $videos);
        }



        return $sitemap->render('xml');
    }
}