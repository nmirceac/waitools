<?php

if(!function_exists('imgSrc')) {
    function imgSrc($imgObject, $options=[]) {
        return \WAI\Cms::imgSrc($imgObject, $options);
    }
}


if(!function_exists('getCapthaField')) {
    function getCapthaField($callbackFunction='captchaCompleted') {
        return \WAI\Form::getCapthaField($callbackFunction);
    }
}


