@php
$view = 'waitools::emails.master';
if(\View::exists('emails.master')) {
    $view = 'emails.master';
}
@endphp
@extends($view)


@section('title', 'New Website Enquiry')

@section('content')
    <tr>
        <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
            <h1>Hello there!</h1>
            <h3>{{ config('waitools.app.name') }}</h3>
            You have a new message from the website<br><br>
            <h4>Message details:</h4><br>

            <table style="padding: 20px; text-align: left; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                <tr>
                    <td>Name:</td>
                    <td>{{ $form['name'] }}</td>
                </tr>

                <tr>
                    <td>Email:</td>
                    <td>{{ $form['email'] }}</td>
                </tr>

                <tr>
                    <td>Phone:</td>
                    <td>{{ $form['phone'] }}</td>
                </tr>

                <tr>
                    <td>Message:</td>
                    <td colspan="2">{{ $form['message'] }}</td>
                </tr>
            </table>

            <br><br>

            <!-- Button : BEGIN -->
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                <tr>
                    <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                        <a href="mailto:{{ $form['email'] }}" style="background: {{ config('waitools.mail.color') }}; border: 15px solid {{ config('waitools.mail.color') }}; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                            &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff;">Write back to {{ $form['email'] }}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                        </a>
                    </td>
                </tr>
            </table>
            <!-- Button : END -->
        </td>
    </tr>
@endsection
