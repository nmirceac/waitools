<!DOCTYPE html>
<html lang="en">
<head>
    @php
        $resources = 'waitools::emails.resources';
        if(\View::exists('emails.resources')) {
            $resources = 'emails.resources';
        }
    @endphp
    @include($resources)
</head>
<body width="100%" bgcolor="#ffffff" style="margin: 0; mso-line-height-rule: exactly;">
<center style="width: 100%; background: #ffffff; text-align: left;">

    <!-- Visually Hidden Preheader Text : BEGIN -->
    <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
        @if (empty($__env->yieldContent('preHeaderText')))
            New email from {{ config('waitools.app.name') }}
        @else
            @yield('preHeaderText')
        @endif
    </div>
    <!-- Visually Hidden Preheader Text : END -->

    <!-- Email Header : BEGIN -->
        <table role="presentation" aria-hidden="true" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 20px 0; text-align: center">
                    @if (empty($__env->yieldContent('header')))
                    <a href="{{ route('home') }}">
                        @if(file_exists(public_path(config('waitools.mail.logo'))))
                            <img src="{{ $message->embed(public_path(config('waitools.mail.logo'))) }}"
                                 aria-hidden="true" width="200" height="50" alt="{{ config('waitools.app.name') }}" border="0" style="height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                        @endif
                    </a>
                    @else
                        @yield('header')
                        @endif
                </td>
            </tr>
        </table>
    <!-- Email Header : END -->


    <!-- Email Body : BEGIN -->
    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

        <!-- Hero Image, Flush : BEGIN -->
        <tr>
            <td bgcolor="#ffffff">
                @if (empty($__env->yieldContent('hero')))
                <a href="{{ route('home') }}">
                    @if(file_exists(public_path(config('waitools.mail.cover'))))
                        <img src="{{ $message->embed( public_path(config('waitools.mail.cover')) ) }}"
                             aria-hidden="true" width="600" height="" alt="{{ config('waitools.app.name') }}" border="0" align="center" style="width: 100%; max-width: 600px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" class="g-img">
                    @endif
                </a>
                @else
                    @yield('hero')
                    @endif
            </td>
        </tr>
        <!-- Hero Image, Flush : END -->

        <!-- 1 Column Text + Button : BEGIN -->
        @yield('content')
        <!-- 1 Column Text + Button : END -->

        <!-- 1 Column Text : BEGIN -->
        <tr>
            <td bgcolor="#ffffff">
                <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                        <td style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;">
                            @if (empty($__env->yieldContent('preFooterText')))
                            {{ config('waitools.app.name') }} - &copy; {{ date('Y') }}.
                            @else
                                @yield('preFooterText')
                                @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- 1 Column Text : END -->


    </table>
    <!-- Email Body : END -->

    <!-- Email Footer : BEGIN -->
    <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
        <tr>
            <td style="width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors">
                @if (empty($__env->yieldContent('footer')))
                    <a href="{{ route('home') }}">{{ config('waitools.app.name') }}</a> |
                    <a href="mailto:{{ config('waitools.app.email') }}">{{ config('waitools.app.email') }}</a>
                @else
                    @yield('footer')
                    @endif
            </td>
        </tr>
    </table>
    <!-- Email Footer : END -->

</center>
</body>
</html>