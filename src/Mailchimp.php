<?php namespace WAI;

use WAI\Exception;

class Mailchimp
{
    public static $instance = null;
    public static $checkedLists = [];
    private $api_key;
    private $api_endpoint = 'https://<dc>.api.mailchimp.com/3.0';
    private $verify_ssl   = true;

    public function __construct()
    {
        $this->api_key = config('waitools.mailchimp.key');
        list(, $datacentre) = explode('-', $this->api_key);
        $this->api_endpoint = str_replace('<dc>', $datacentre, $this->api_endpoint);
        return $this;
    }

    public function delete($method, $args=array(), $timeout=10)
    {
        return $this->makeRequest('delete', $method, $args, $timeout);
    }

    public function get($method, $args=array(), $timeout=10)
    {
        return $this->makeRequest('get', $method, $args, $timeout);
    }

    public function patch($method, $args=array(), $timeout=10)
    {
        return $this->makeRequest('patch', $method, $args, $timeout);
    }

    public function post($method, $args=array(), $timeout=10)
    {
        return $this->makeRequest('post', $method, $args, $timeout);
    }

    public function put($method, $args=array(), $timeout=10)
    {
        return $this->makeRequest('put', $method, $args, $timeout);
    }

    /**
     * Performs the underlying HTTP request. Not very exciting
     * @param  string $http_verb   The HTTP verb to use: get, post, put, patch, delete
     * @param  string $method       The API method to be called
     * @param  array  $args         Assoc array of parameters to be passed
     * @return array|boolean        Assoc array of decoded result
     * @throws
     */
    private function makeRequest($http_verb, $method, $args=array(), $timeout=10)
    {
        $url = $this->api_endpoint.'/'.$method;

        $json_data = json_encode($args, JSON_FORCE_OBJECT);

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/vnd.api+json',
                'Content-Type: application/vnd.api+json',
                'Authorization: apikey '.$this->api_key));
            curl_setopt($ch, CURLOPT_USERAGENT, 'WAITools/MailChimp-API/3.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);


            switch($http_verb) {
                case 'post':
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'get':
                    $query = http_build_query($args);
                    curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
                    break;

                case 'delete':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;

                case 'patch':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'put':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;
            }


            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        return $result ? json_decode($result, true) : false;
    }

    public static function callApi($method, $endpoint, $data = [])
    {
        try {
            $response = self::$instance->$method($endpoint, $data);
        } catch (Exception $e) {
            throw new Exception('Mailchip exception: ' . $e->getMessage());
        }
        if ($response === false) {
            throw new Exception('Error in Mailchimp - possible connectivity problem');
        }
        return $response;
    }

    public static function getInstance()
    {
        if(is_null(self::$instance)) {
            self::$instance = new Mailchimp();
        }

        return self::$instance;
    }

    public static function check($emailAddress, $listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        $result = self::checkStatus($emailAddress, $listId);
        if($result == 'subscribed' || $result == 'pending') {
            return true;
        }
        return false;
    }

    /**
     * Checks the status of a list subscriber
     * Possible statuses: 'subscribed', 'unsubscribed', 'cleaned', 'pending', or 'not found'
     * @param $emailAddress
     * @param $listId
     * @return string
     */
    public static function checkStatus($emailAddress, $listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        // Check the list exists
        if(!self::checkListExists($listId)) {
            throw new Exception('Mailchimp checkStatus called on a list that does not exist (' . $listId . ')');
        }
        // Check whether the list has the subscriber
        $id = md5(strtolower($emailAddress));
        $endpoint = "lists/{$listId}/members/{$id}";
        $response = self::callApi('get', $endpoint);
        if (empty($response['status'])) {
            throw new Exception('Mailchimp checkStatus return value did not contain status for list '.$listId.' and subscriber '.$emailAddress);
        }
        if ($response['status'] == 404) {
            $response['status'] = 'not found';
        }
        return $response['status'];
    }

    /**
     * @param $listId
     * @return bool
     * @throws MailchimpException
     */
    public static function checkListExists($listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        if(isset(self::$checkedLists[$listId])) {
            return self::$checkedLists[$listId];
        }

        $endpoint = "lists/{$listId}";
        $response = self::callApi('get', $endpoint);
        if (!empty($response['status']) && $response['status'] == 404) {
            self::$checkedLists[$listId] = false;
        } else {
            self::$checkedLists[$listId] = true;
        }

        return self::$checkedLists[$listId];
    }

    public static function subscribe($emailAddress, $mergeFields = [], $confirm = true, $listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        // Check the list exists
        if(!self::checkListExists($listId)) {
            throw new Exception('Mailchimp subscribe called on list that does not exist: ' . $listId);
        }
        // Check address is valid for subscription
        $status = self::checkStatus($emailAddress, $listId);
        if (in_array($status, ['subscribed', 'pending', 'cleaned'])) {
            return false;
        }
        // Add/update the subscriber - PUT does both
        $id = md5(strtolower($emailAddress));
        $endpoint = "lists/{$listId}/members/{$id}";
        $status = $confirm ? 'pending' : 'subscribed';
        $data = [
            'email_address' => $emailAddress,
            'status' => $status
        ];
        if(!empty($mergeFields)) {
            $data['merge_fields'] = $mergeFields;
        }
        $response = self::callApi('put', $endpoint, $data);
        if (empty($response['status']) || !in_array($response['status'], ['subscribed', 'pending'])) {
            throw new Exception('Mailchimp subscribe received unexpected response from Mailchimp: ' . json_encode($response));
        }

        return true;
    }

}
