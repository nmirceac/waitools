<?php namespace WAI;

use WAI\Exception;

class Nuz
{
    public static $instance = null;
    public static $checkedLists = [];
    private $api_prefix;
    private $api_key;
    private $list_id;

    public function __construct()
    {
        $this->api_prefix = config('waitools.nuz.url').'/api';
        $this->api_key = config('waitools.nuz.key');
        $this->list_id = config('waitools.nuz.list');
        return $this;
    }

    public static function getInstance()
    {
        if(is_null(self::$instance)) {
            self::$instance = new Nuz();
        }

        return self::$instance;
    }

    private function makeRequest($http_verb, $method, $args=array(), $timeout=10)
    {
        $url = $this->api_prefix.'/'.$method;

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/vnd.api+json',
                'Content-Type: application/vnd.api+json'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'WAITools/NUZ-API/1.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

            if(!isset($args['key'])) {
                $args['key'] = $this->api_key;
            }
            $json_data = json_encode($args, JSON_FORCE_OBJECT);

            switch($http_verb) {
                case 'post':
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'get':
                    $query = http_build_query($args);
                    curl_setopt($ch, CURLOPT_URL, $url.'?'.$query);
                    break;

                case 'delete':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                    break;

                case 'patch':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;

                case 'put':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
                    break;
            }

            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        return $result ? json_decode($result, true) : false;
    }

    public function callApi($method, $endpoint, $data = [])
    {
        try {
            $response = $this->makeRequest($method, $endpoint, $data);
        } catch (Exception $e) {
            throw new Exception('NUZ exception: ' . $e->getMessage());
        }
        if ($response === false) {
            throw new Exception('Error in NUZ - possible connectivity problem');
        }
        return $response;
    }

    public static function check($emailAddress, $listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        $result = self::checkStatus($emailAddress, $listId);
        if($result == 'subscribed' || $result == 'pending') {
            return true;
        }
        return false;
    }

    /**
     * Checks the status of a list subscriber
     * Possible statuses: 'subscribed', 'unsubscribed', 'cleaned', 'pending', or 'not found'
     * @param $emailAddress
     * @param $listId
     * @return string
     */
    public static function checkStatus($emailAddress, $listId=null)
    {
        if(is_null($listId)) {
            $listId = config('waitools.mailchimp.list');
        }

        // Check the list exists
        if(!self::checkListExists($listId)) {
            throw new Exception('Nuz checkStatus called on a list that does not exist (' . $listId . ')');
        }
        // Check whether the list has the subscriber
        $id = md5(strtolower($emailAddress));
        $endpoint = "lists/{$listId}/members/{$id}";
        $response = self::callApi('get', $endpoint);
        if (empty($response['status'])) {
            throw new Exception('Nuz checkStatus return value did not contain status for list '.$listId.' and subscriber '.$emailAddress);
        }
        if ($response['status'] == 404) {
            $response['status'] = 'not found';
        }
        return $response['status'];
    }

    public function subscribe($emailAddress, $details = [], $listId=null)
    {
        if(is_null($listId)) {
            $listId = $this->list_id;
        }

        $payload['listId']=$listId;
        $payload['subscribers'][$emailAddress] = $details;


        $response = $this->callApi('post', 'subscribe', $payload);

        if(isset($response['message'])) {
            throw new Exception('Nuz Api problem: ' . json_encode($response));
        }

        if(count($response['added'])>0) {
            return 'Added';
        }

        else if(count($response['invalid'])>0) {
            return 'Invalid email address';
        }

        else if(count($response['existing'])>0) {
            if(count($response['updated'])>0) {
                return 'Updated';
            } else {
                return 'Already on the list';
            }
        }

        return true;
    }

}
