"use strict";
var page = require('webpage').create(),
    system = require('system'),
    address, output, svgWidth, svgHeight, background, trim;

if (system.args.length < 3 || system.args.length > 7) {
    console.log('Usage: rasterize.js URL filename width height background color');
    console.log('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    console.log('  image (png/jpg output) examples: "1920px" entire page, window width 1920px');
    console.log('                                   "800px*600px" window, clipped to 800x600');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    svgWidth = parseInt(system.args[3]);
    svgHeight = parseInt(system.args[4]);
    background = '#'+system.args[5];
    if(system.args[6]) {
        trim = true;
    } else {
        trim = false;
    }
    page.viewportSize = { width: svgWidth, height:svgHeight };

    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('Unable to load the address!');
            phantom.exit(1);
        } else {
            var height = page.evaluate(function(background) {
                document.getElementsByTagName('svg')[0].style.backgroundColor=background;
                // document.getElementsByTagName('svg')[0].style.height='100px';
                // var object = document.getElementsByTagName('svg')[0].height.baseVal;
                var object = document.getElementsByTagName('svg')[0].style.height;
                return object;
            }, background);

            console.log(height);

            window.setTimeout(function () {
                if(trim) {
                    page.clipRect = { top:3, left:3, width: svgWidth-3, height: svgHeight-6};
                }
                page.render(output, {format: 'jpeg', quality: '97'});
                phantom.exit();
            }, 200);
        }
    });
}