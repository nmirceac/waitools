<?php namespace WAI;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Tools
 * @package WAI
 */
class Tools
{
    public function clearCache()
    {
        \Artisan::call('cache:clear');
        return response('<meta http-equiv="refresh" content="3;URL=\'/\'" />All done. Going to the homepage now!');
    }
}