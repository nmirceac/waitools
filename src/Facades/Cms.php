<?php namespace WAI\Facades;
use Illuminate\Support\Facades\Facade;

class Cms extends Facade
{
    public static $cacheMinutes = 10;

    public static $instance = null;
    public static $application = null;
    public static $adverts = null;

    protected static function getFacadeAccessor()
    {
        return 'cms';
    }

    public static function getInstance()
    {
        if(is_null(self::$instance)) {
            self::$instance = new \WAI\Cms();
            self::$application = (object) self::$instance->getApplication();
            self::$adverts = self::$instance->getAdverts();
        }

        return self::$instance;
    }

    public static function getApplication()
    {
        return self::$application;
    }

    public static function getAdverts()
    {
        return self::$adverts;
    }

    public static function getAdvertByZone($zoneId)
    {
        if(isset(self::$adverts[$zoneId])) {
            return self::$adverts[$zoneId];
        } else {
            return '';
        }
    }

    public static function getApplications()
    {
        return self::$instance->getApplications();
    }

    public static function articlesByParentId($parentId=0, $options=[])
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::$instance->getArticlesByParentId($parentId, $options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes(self::$cacheMinutes));
        }
        return $data;
    }

    public static function categoryByCategoryPath($categoryPath, $options=[])
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::$instance->getCategoryByCategoryPath($categoryPath, $options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes(self::$cacheMinutes));
        }
        return $data;
    }

    public static function articlesByParentIdCached($parentId=0, $options=[], $cacheMinutes=30)
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::articlesByParentId($parentId, $options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes($cacheMinutes));
        }
        return $data;
    }

    public static function getTrendingArticles($options=[])
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::$instance->getTrendingArticles($options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes(self::$cacheMinutes));
        }
        return $data;
    }

    public static function getCategories($parentId=0, $options=[])
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::$instance->getCategories($parentId, $options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes(self::$cacheMinutes));
        }
        return $data;
    }

    public static function getCategoriesAndSubcategories($parentId=0, $options=[])
    {
        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $data = \Cache::get($cacheKey);
        if(is_null($data)) {
            $data = self::$instance->getCategoriesAndSubcategories($parentId, $options);
            \Cache::put($cacheKey, $data, \Carbon\Carbon::now()->addMinutes(self::$cacheMinutes));
        }
        return $data;
    }



    /**
     * @param $imgObject
     * @param array $options
     * @return string
     */
    public static function imgSrc($imgObject, $options=[]) {
        return \WAI\Cms::imgSrc($imgObject, $options);
    }
}

Cms::getInstance();
