<?php namespace WAI\Facades;

use Illuminate\Support\Facades\Facade;

class Issues extends Facade
{
    public static $instance = null;

    protected static function getFacadeAccessor()
    {
        return 'issues';
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new \WAI\Issues();
        }

        return self::$instance;
    }

    public static function getMags($identifier=null, $covers=false)
    {
        return self::$instance->getMags($identifier, $covers);
    }
}

Issues::getInstance();