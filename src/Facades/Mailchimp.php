<?php namespace WAI\Facades;

use Illuminate\Support\Facades\Facade;

class Mailchimp extends Facade
{
    public static $instance = null;

    protected static function getFacadeAccessor()
    {
        return 'mailchimp';
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = \WAI\Mailchimp::getInstance();
        }

        return self::$instance;
    }

    public static function check($emailAddress, $listId=null)
    {
        return \WAI\Mailchimp::check($emailAddress, $listId);
    }

    public static function checkStatus($emailAddress, $listId=null)
    {
        return \WAI\Mailchimp::checkStatus($emailAddress, $listId);
    }

    public static function checkListExists($listId=null)
    {
        return \WAI\Mailchimp::checkListExists($listId);
    }

    public static function subscribe($emailAddress, $mergeFields = [], $confirm = true, $listId=null)
    {
        return \WAI\Mailchimp::subscribe($emailAddress, $mergeFields, $confirm, $listId);
    }
}

Mailchimp::getInstance();