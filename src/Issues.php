<?php namespace WAI;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Issues
 * @package WAI
 */
class Issues
{
    private $url, $client, $secret;

    /**
     * @var null|\WAI\Api
     */
    public $instance = null;
    /**
     * @var null
     */
    public $application = null;

    /**
     * Issues constructor.
     */
    function __construct()
    {
        $this->url = config('waitools.issues.url');
        $this->client = config('waitools.issues.username');
        $this->secret = config('waitools.issues.password');

        $this->instance = new Api($this->url, $this->client, $this->secret, [
            'debug' => false,
            'load_constants' => false
        ]);
    }

    /**
     * @param string $identifier
     * @param bool $covers
     */
    function cacheMags($identifier=null, $covers=false)
    {
        if(is_null($identifier)) {
            $identifier = config('waitools.issues.identifier');
        }

        $cacheKey = 'getMags-'.md5(json_encode(func_get_args()));
        $cache = $this->instance->getMags($identifier, $covers);
        Cache::put($cacheKey, $cache, Carbon::now()->addHours(config('waitools.issues.cache', 24)));
    }

    /**
     * @param string $identifier
     * @param bool $covers
     * @return mixed
     */
    function getMags($identifier=null, $covers=false)
    {
        if(is_null($identifier)) {
            $identifier = config('waitools.issues.identifier');
        }

        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $cache = Cache::get($cacheKey);
        if(is_null($cache)) {
            $cache = $this->instance->getMags($identifier, $covers);
            Cache::put($cacheKey, $cache, Carbon::now()->addHours(config('waitools.issues.cache', 24)));
        }
        return $cache;
    }

    /**
     * @param string $identifier
     * @return mixed
     */
    function getLastMag($identifier=null)
    {
        if(is_null($identifier)) {
            $identifier = config('waitools.issues.identifier');
        }

        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $cache = Cache::get($cacheKey);
        if(is_null($cache)) {
            $lastMag = $this->instance->getLastMag($identifier);
            $lastMag['cover'] = base64_decode($lastMag['cover']);
            $lastMag['coverBig'] = base64_decode($lastMag['coverBig']);
            $cache = $lastMag;
            Cache::put($cacheKey, $cache, Carbon::now()->addHours(config('waitools.issues.cache', 24)));
        }
        return $cache;
    }

    /**
     * @param int $issueId
     * @param string $identifier
     * @return mixed
     */
    function getMagCover($issueId=0, $identifier=null)
    {
        if(is_null($identifier)) {
            $identifier = config('waitools.issues.identifier');
        }

        $localStorePath = storage_path('issues');
        if(!file_exists($localStorePath)) {
            mkdir($localStorePath);
        }

        $coverPath = $localStorePath.'/'.$identifier.'-'.$issueId.'.jpg';
        $coverBigPath = $localStorePath.'/'.$identifier.'-'.$issueId.'-big.jpg';

        if(!file_exists($coverPath) and !file_exists($coverBigPath)) {
            $lastMag=$this->instance->getMag($identifier,  $issueId);
            $covers['cover'] = base64_decode($lastMag['cover']);
            $covers['coverBig'] = base64_decode($lastMag['coverBig']);

            if(!empty($covers['cover'])) {
                file_put_contents($coverPath, $covers['cover']);
            }

            if(!empty($covers['coverBig'])) {
                file_put_contents($coverBigPath, $covers['coverBig']);
            }
        } else {
            $covers['cover'] = file_get_contents($coverPath);
            $covers['coverBig'] = file_get_contents($coverBigPath);
        }

        return $covers;
    }

    /**
     * @param string $identifier
     * @return mixed
     */
    function getLastMagId($identifier=null)
    {
        if(is_null($identifier)) {
            $identifier = config('waitools.issues.identifier');
        }

        $cacheKey = __FUNCTION__.'-'.md5(json_encode(func_get_args()));
        $cache = Cache::get($cacheKey);
        if(is_null($cache)) {
            $cache = $this->instance->getLastMagId($identifier);
            Cache::put($cacheKey, $cache, Carbon::now()->addHours(config('waitools.issues.cache', 24)));
        }
        return $cache;
    }
}