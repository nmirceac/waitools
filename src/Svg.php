<?php namespace WAI;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Svg
 * @package WAI
 */
class Svg
{
    private $path = null;

    private $svgContent = null;

    private $colors = ['#000000'];

    public function __construct($path = null)
    {
        if(!is_null($path)) {
            $this->setPath($path);
        } else {
            $this->setPath(storage_path('svg'));
        }
    }

    public function loadSvg($svgFile)
    {
        if(!file_exists($this->path.'/'.$svgFile)) {
            throw new Exception('No svg file was found at this path - '.$this->path.'/'.$svgFile);
        }

        $this->svgContent = file_get_contents($this->path.'/'.$svgFile);
        return $this;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function getColors()
    {
        return $this->colors;
    }

    public function setColors($colors)
    {
        if(!is_array($colors)) {
            $colors = explode(',', $colors);
        }

        foreach ($colors as $colorIndex=>$color) {
            if(substr($color, 0, 1)!=='#') {
                $colors[$colorIndex] = '#'.$color;
            }
        }

        $this->colors = $colors;
        return $this;
    }

    public function getSvgContent()
    {
        return $this->svgContent;
    }

    public function processSvg($colors = null)
    {
        if(!is_null($colors) and is_array($colors)) {
            $this->setColor($colors);
        }

        $toReplace = [];
        foreach($this->getColors() as $colorIndex=>$color) {
            $toReplace[] = '#00000'.$colorIndex;
        }

        return str_replace($toReplace, $this->colors, $this->svgContent);
    }

    public static function convertSvgToColor($svgFilename, $colors=['#000000'])
    {
        return (new Svg())->loadSvg($svgFilename)->setColors($colors)->processSvg();
    }

    public function controllerSvg($svgFilename, $colors)
    {
        $svgFile = $svgFilename;
        if(substr($svgFilename, -4)!='.svg') {
            $svgFilename.='.svg';
        }

        $colors = explode('-', $colors);

        $requestUri = request()->getRequestUri();

        $svg = \WAI\Svg::convertSvgToColor($svgFilename, $colors);
        if(!file_exists(public_path('svg/'.$svgFile))) {
            mkdir(public_path('svg/'.$svgFile));
        }

        file_put_contents(public_path($requestUri), $svg);

        return redirect($requestUri);
    }

    public function controllerJpeg($svgFilename, $colors)
    {
        $phantomJsBin = '../node_modules/phantomjs/bin/phantomjs';
        if(!file_exists($phantomJsBin)) {
            throw new Exception('No phantomJs binary. Just npm install --save phantomjs and you\'re good to go');
        }

        $rasterizeScript = $phantomJsBin.' --ignore-ssl-errors=true --ssl-protocol=any '.dirname(__FILE__).'/js/rasterize.js';

        $svgFile = $svgFilename;

        $colors = explode('-', $colors);

        $requestUri = request()->getRequestUri();

        $width = array_pop($colors);
        if(substr($width, -1)=='t') {
            $trim = true;
            $width = substr($width, 0, -1);
        } else {
            $trim = false;
        }

        $background = array_pop($colors);

        $svgRequestUri = '/svg/'.$svgFile.'/'.implode('-', $colors).'.svg';
        $svgPath = config('app.url').$svgRequestUri;
        $svgContents = file_get_contents($svgPath);

        preg_match("/viewBox=\"\S+ \S+ (\S+) (\S+)\"/i", $svgContents, $svgInfo);
        $svgWidth = ceil($svgInfo[1]);
        $svgHeight = ceil($svgInfo[2]);

        $height = ceil($width/$svgWidth * $svgHeight);

        if($trim) {
            exec($rasterizeScript.' '.$svgPath.' '.public_path($requestUri).' '.$width.' '.$height.' '.$background.' trim', $output);
        } else {
            exec($rasterizeScript.' '.$svgPath.' '.public_path($requestUri).' '.$width.' '.$height.' '.$background, $output);
        }

//        uncomment for debugging
//        $contents = file_get_contents(public_path($requestUri));
//        unlink(public_path($requestUri));

        return redirect($requestUri);
    }
}