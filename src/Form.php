<?php namespace WAI;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Svg
 * @package WAI
 */
class Form
{
    public static function getCapthaField($callbackFunction='captchaCompleted')
    {
        $recaptchaKey = config('waitools.recaptcha.key');
        if($recaptchaKey) {
            return self::getCapthaJs().'<div class="g-recaptcha" data-sitekey="'.$recaptchaKey.'" data-callback="'.$callbackFunction.'"></div>';
        }
    }

    public static function getCapthaJs()
    {
        return '<script src="https://www.google.com/recaptcha/api.js"></script>';
    }

    public static function verifyCaptchaReponse($response)
    {
        $recaptchaSecret = config('waitools.recaptcha.secret');

        if (function_exists('curl_init') && function_exists('curl_setopt')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($ch, CURLOPT_USERAGENT, 'WAITools/Form');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($ch, CURLOPT_POST, true);

            $data = [
                'secret' => $recaptchaSecret,
                'response' => $response,
            ];

            if(isset($_SERVER['REMOTE_ADDR']) and !empty($_SERVER['REMOTE_ADDR'])) {
                $data['remoteip'] = $_SERVER['REMOTE_ADDR'];
            }

            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            throw new \Exception("cURL support is required, but can't be found.");
        }

        $result = json_decode(trim($result));
        if(isset($result->success) and $result->success) {
            return true;
        } else {
            return false;
        }
    }

    public function submitForm()
    {
        $recaptchaSecret = config('waitools.recaptcha.secret');
        if($recaptchaSecret) {
            $response = request()->get('g-recaptcha-response');
            $verify = self::verifyCaptchaReponse($response);
        }

        if(!$verify) {
            return response()->json([
                'error' => 'Invalid captcha code'
            ]);
        }

        $form = request()->except(['g-recaptcha-response', '_token']);

        $validator = \Validator::make($form, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|digits_between:7,14',
            'message' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->messages()->toArray()
            ]);
        } else {
            $to['email'] = \Config::get('waitools.app.email');
            $to['name'] = \Config::get('waitools.app.name');

            dispatch(new \WAI\Jobs\SendContactFormEmail($form, $to));

            $message = 'Your message was sent successfully';

            return response()->json([
                'message' => $message
            ]);
        }

        return response()->json(['verify'=>$verify]);
    }
}

